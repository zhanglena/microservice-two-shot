from django.shortcuts import render
from .models import BinVO, Shoe
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json



class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "bin_number", "bin_size", "import_href"]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer", "model_name", "color", "picture_url", "id"]
    # encoders = {"bins": BinVODetailEncoder()}

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer", "model_name", "color", "picture_url", "id", "bins"]
    encoders = {"bins": BinVODetailEncoder()}

@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse({"shoes": shoes}, encoder=ShoeListEncoder, safe=False,)
    else:
        content=json.loads(request.body)
        try:
            shoe_href = content["bins"]
            bins = BinVO.objects.get(import_href=shoe_href)
            content["bins"] = bins
            print("hello", content)
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
        shoe,
        encoder=ShoeDetailEncoder,
        safe=False,
    )

@require_http_methods(["GET","PUT","DELETE"])
def api_detail_shoe(request, pk):

    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        # try:
        shoe_href = content["bins"]
        bins = BinVO.objects.get(import_href=shoe_href)
        content["bins"] = bins
        print("hello", content)
        # except BinVO.DoesNotExist:
        #     return JsonResponse(
        #         {"message": "Invalid bin id"},
        #         status=400,
        #     )
        Shoe.objects.filter(id=pk).update(**content)
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

def api_list_bins(request):
    if request.method == "GET":
        bins = BinVO.objects.all()
        return JsonResponse({"bins": bins}, encoder=BinVODetailEncoder, safe=False,)
