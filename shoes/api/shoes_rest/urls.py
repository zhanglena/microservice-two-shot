from django.urls import path

from .views import api_list_shoes, api_detail_shoe, api_list_bins

urlpatterns = [
    path("shoes/", api_list_shoes, name="api_create_shoes"),
    path("shoes/<int:pk>/", api_detail_shoe, name="api_detail_shoe"),
    path("bins/", api_list_bins, name="bin_vo_list"),
]
