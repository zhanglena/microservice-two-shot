from django.db import models

class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=150)
    model_name= models.CharField(max_length=150)
    color = models.CharField(max_length=150)
    picture_url = models.URLField()
    bins = models.ForeignKey(BinVO, related_name="shoe", on_delete=models.CASCADE)

    def __str__(self):
        return self.model_name
