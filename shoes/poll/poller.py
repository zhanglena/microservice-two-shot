import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

from shoes_rest.models import BinVO
# Import models from hats_rest, here.
# from shoes_rest.models import Something



def get_bin():
    url = "http://wardrobe-api:8000/api/bins/"
    response = requests.get(url)
    print(response)
    content = json.loads(response.content)
    for bins in content["bins"]:
        BinVO.objects.update_or_create(import_href=bins["href"],
        defaults={"closet_name": bins["closet_name"],
        "bin_number": bins["bin_number"], "bin_size": bins["bin_size"]})



def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            # Write your polling logic, here
            get_bin()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
