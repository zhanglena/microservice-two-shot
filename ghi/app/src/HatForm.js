import React from 'react';

class HatForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      style_name: '',
      fabric: '',
      color: '',
      picture_url: '',
      location: '',
      locations: [],
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeLocation = this.handleChangeLocation.bind(this);
    this.handleChangePictureURL = this.handleChangePictureURL.bind(this);
    this.handleChangeColor = this.handleChangeColor.bind(this);
    this.handleChangeFabric = this.handleChangeFabric.bind(this);
    this.handleChangeStyleName = this.handleChangeStyleName.bind(this);
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ locations: data.locations });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.locations;

    const locationUrl = 'http://localhost:8090/api/hats/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newHat = await response.json();
      console.log(newHat);
      this.setState({
        style_name: '',
        fabric: '',
        color: '',
        picture_url: '',
        location: '',
      });
    }
  }

  handleChangeLocation(event) {
    const value = event.target.value;
    this.setState({ location: value });
  }

  handleChangePictureURL(event) {
    const value = event.target.value;
    this.setState({ picture_url: value });
  }

  handleChangeColor(event) {
    const value = event.target.value;
    this.setState({ color: value });
  }

  handleChangeFabric(event) {
    const value = event.target.value;
    this.setState({ fabric: value });
  }

  handleChangeStyleName(event) {
    const value = event.target.value;
    this.setState({ style_name: value });
  }


  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat</h1>
            <form onSubmit={this.handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
                <input onChange={this.handleStyleName} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control" />
                <label htmlFor="style_name">Style Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeMaxFabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                <label htmlFor="fabric">Fabric</label>
              </div>
               <div className="mb-3">
                <select onChange={this.handleChangeLocation} value={this.state.location} id="location" className="form-select">
                  <option value="">Choose a location</option>
                  {this.state.locations.map(location => {
                    return (
                      <option key={location.id} value={location.id}>{location.closet_name}</option>
                    )
                  })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handlePictureURL} placeholder="Picture" required type="url" name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="picture_url">Picture Link</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default HatForm;
