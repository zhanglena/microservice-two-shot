import React from "react";
import { Link } from "react-router-dom";

function ShoeColumn(props) {
  return (
    <div className="col">
      {props.list.map((data) => {
        return (
          <div key={data.id} className="card mb-3 shadow">
            <img src={data.picture_url} className="card-img-top" />
            <div className="card-body">
              <h5 className="card-title">{data.model_name}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                {data.manufacturer}
              </h6>
              <p className="card-text">{data.color}</p>
            </div>
            <div className="card-footer">
              {data.bins.closet_name}, Bin # {data.bins.bin_number}
            </div>
            <form className="card-footer">
              <button
                class="btn btn-outline-danger"
                onClick={() => props.delete(data)}
              >
                Delete
              </button>
            </form>
          </div>
        );
      })}
    </div>
  );
}

class ShoeList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      shoeColumns: [[], [], []],
    };
  }

  async componentDidMount() {
    const url = "http://localhost:8080/api/shoes/";

    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const requests = [];
        for (let shoe of data.shoes) {
          const detailUrl = `http://localhost:8080/api/shoes/${shoe.id}/`;
          requests.push(fetch(detailUrl));
        }

        const responses = await Promise.all(requests);

        const shoeColumns = [[], [], []];

        let i = 0;
        for (const shoeResponse of responses) {
          if (shoeResponse.ok) {
            const details = await shoeResponse.json();
            shoeColumns[i].push(details);
            i = i + 1;
            if (i > 2) {
              i = 0;
            }
          } else {
            console.error(shoeResponse);
          }
        }

        this.setState({ shoeColumns: shoeColumns });
      }
    } catch (e) {
      console.error(e);
    }

    // const dlt = async (event) => {
    //   const url = `http://localhost:8080/api/shoes/${data.id}/`;
    //   const fetchConfig = {
    //     method: "delete",
    //     headers: { "Content-Type": "application/json" },
    //   };
    //   const response = await fetch(url, fetchConfig);
    //   if (response.ok) {
    //     console.log({ Success: "cancelled appointment" });
    //   } else {
    //     console.log({ "Not Success my dude": "you failed" });
    //   }
  }
  delete = async (shoe_id) => {
    // event.preventDefault();
    console.log(shoe_id);
    const deleteUrl = `http://localhost:8080/api/shoes/${shoe_id.id}/`;
    const fetchConfig = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(deleteUrl, fetchConfig);
    if (response.ok) {
      let filteredShoeColumns = [
        ...[
          this.state.shoeColumns[0].filter(
            (shoe) => shoe.href !== shoe_id.href
          ),
        ],
        ...[
          this.state.shoeColumns[1].filter(
            (shoe) => shoe.href !== shoe_id.href
          ),
        ],
        ...[
          this.state.shoeColumns[2].filter(
            (shoe) => shoe.href !== shoe_id.href
          ),
        ],
      ];
      this.setState({ shoeColumns: filteredShoeColumns });
    }
  };

  render() {
    return (
      <>
        <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
          <img
            className="bg-white rounded shadow d-block mx-auto mb-4"
            src="/logo.svg"
            alt=""
            width="600"
          />
          <h1 className="display-5 fw-bold">Wardrobe Kicks</h1>
          <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
              When hard work meets the right inspiration, an awesome pair of
              kicks will be born. We created Wardrobify to channel this idea and
              create sneakers that fit perfectly with who you are and help you
              push your comfort zone.
            </p>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
              <Link
                to="/shoes/new"
                className="btn btn-primary btn-lg px-4 gap-3"
              >
                Add your sneakers
              </Link>
            </div>
          </div>
        </div>
        <div className="container">
          <h2>All the shoes</h2>
          <div className="row">
            {this.state.shoeColumns.map((shoeList, index) => {
              return (
                <ShoeColumn delete={this.delete} key={index} list={shoeList} />
              );
            })}
          </div>
        </div>
      </>
    );
  }
}

export default ShoeList;
