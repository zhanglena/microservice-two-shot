import React from 'react';
import { Link } from 'react-router-dom';


function HatColumn(props) {
  return (
    <div className="col">
      {props.list.map(data => {
        return (
          <div key={data.id} className="card mb-3 shadow">
            <img src={data.picture_url} className="card-img-top" />
            <div className="card-body">
              <h5 className="card-title">{data.style_name}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                {data.fabric}
              </h6>
              <p className="card-text">
                {data.location.closet_name}
              </p>
            </div>
            <form className="card-footer">
              <button onClick={() => props.delete(data)}>
                Delete
              </button>
            </form>

          </div>
        );
      })}
    </div>
  );
}

class HatsList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hatColumns: [[], [], []],
    };
  }

  async componentDidMount() {
    const url = 'http://localhost:8090/api/hats/';

    try {
      const response = await fetch(url);
      if (response.ok) {

        const data = await response.json();
        console.log("data", data)


        const requests = [];
        console.log("requests", requests)
        for (let hat of data.hats) {
          const detailUrl = `http://localhost:8090/api/hats/${hat.id}`;
          requests.push(fetch(detailUrl));
        }


        const responses = await Promise.all(requests);
        console.log("responses", responses)


        const hatColumns = [[], [], []];
        console.log("hatColumns", hatColumns)


        let i = 0;
        for (const hatResponse of responses) {
          if (hatResponse.ok) {
            const details = await hatResponse.json();
            hatColumns[i].push(details);
            i = i + 1;
            if (i > 2) {
              i = 0;
            }
          } else {
            console.error(hatResponse);
          }
        }


        this.setState({hatColumns: hatColumns});
      }
    } catch (e) {
      console.error(e);
    }
  }

  delete = async (hat_id) => {
    console.log(hat_id);
    const deleteUrl = `http://localhost:8090/api/hats/${hat_id.id}/`;
    const fetchConfig = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(deleteUrl, fetchConfig);
    if (response.ok) {
      let filteredHatColumns = [
        ...[
          this.state.hatColumns[0].filter(
            (hat) => hat.href !== hat_id.href
          ),
        ],
        ...[
          this.state.hatColumns[1].filter(
            (hat) => hat.href !== hat_id.href
          ),
        ],
        ...[
          this.state.hatColumns[2].filter(
            (hat) => hat.href !== hat_id.href
          ),
        ],
      ];
      this.setState({ hatColumns: filteredHatColumns });
    }
  };



  render() {
    return (
      <>
      <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
          <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
          <h1 className="display-5 fw-bold">Your Hats!</h1>
          <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
              A place to organize all your amazing hats
            </p>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
              <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Add a Hat</Link>
            </div>
          </div>
        </div>
        <div className="container">
          <h2>Hats</h2>
          <div className="row">
            {this.state.hatColumns.map((hatList, index) => {
              return (
                <HatColumn key={index} list={hatList} />
              );
            })}
          </div>
        </div>
      </>
    );
  }
}

export default HatsList;
