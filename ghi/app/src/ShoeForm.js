import React from "react";

class ShoeForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      manufacturer: "",
      model_name: "",
      color: "",
      picture_url: "",
      bins_list: [],
      bins: "",
    };
    this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
    this.handlePictureUrlChange = this.handlePictureUrlChange.bind(this);
    this.handleBinsChange = this.handleBinsChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleManufacturerChange(event) {
    const value = event.target.value;
    this.setState({ manufacturer: value });
  }

  handleNameChange(event) {
    const value = event.target.value;
    this.setState({ model_name: value });
  }

  handleColorChange(event) {
    const value = event.target.value;
    this.setState({ color: value });
  }

  handlePictureUrlChange(event) {
    const value = event.target.value;
    this.setState({ picture_url: value });
  }

  handleBinsChange(event) {
    const value = event.target.value;
    this.setState({ bins: value });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.bins_list;
    console.log(data);

    const shoeUrl = "http://localhost:8080/api/shoes/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(shoeUrl, fetchConfig);
    if (response.ok) {
      const newBin = await response.json();
      console.log(newBin);

      const cleared = {
        manufacturer: "",
        name: "",
        color: "",
        picture_url: "",
        bins: "",
      };
      this.setState(cleared);
    }
  }
  async componentDidMount() {
    const url = "http://localhost:8100/api/bins";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ bins_list: data.bins });
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Shoe</h1>
            <form onSubmit={this.handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleManufacturerChange}
                  placeholder="manufacturer"
                  required
                  type="text"
                  name="manufacturer"
                  id="manufacturer"
                  className="form-control"
                  value={this.state.manufacturer}
                />
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleNameChange}
                  placeholder="name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                  value={this.state.model_name}
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleColorChange}
                  placeholder="color"
                  required
                  type="text"
                  name="color"
                  id="color"
                  className="form-control"
                  value={this.state.color}
                />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <textarea
                  onChange={this.handlePictureUrlChange}
                  placeholder="picture_url"
                  required
                  type="text"
                  name="picture_url"
                  id="picture_url"
                  className="form-control"
                  value={this.state.picture_url}
                />
                <label htmlFor="picture_url">Picture URL</label>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleBinsChange}
                  value={this.state.bins}
                  required
                  name="bin"
                  id="bin"
                  className="form-select"
                >
                  <option value="">Choose a Bin</option>
                  {this.state.bins_list.map((bin) => {
                    return (
                      <option key={bin.href} value={bin.href}>
                        {bin.closet_name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ShoeForm;
