# Wardrobify

Team:

- Person 1 - Which microservice?
- Cara- Hats

* Person 2 - Which microservice?
* Lena Zhang - Shoe's Microservice

## Design

1. Complete Models/Views/Url for Hats/Shoes Microservice
2. Poll Location/Bin
3. React frontend to show the shoes/hats and implementing a creating form
4. Create delete

## Shoes microservice

The shoes microservice on port 8080 will contain the models, views, and urls to access the individual shoes and detail instances from the shoe model. It will include a BinVO that contains bin href on every shoe id. This will poll the bin information from the wardrobe microservice to the shoe microservice through polling so that the shoe knows exactly which bin it is currently in. On the react frontend, we will use functional and class based components to enable a SPA that shows a list of the shoes on Port 3000 which calls the shoe api for the shoe details.

## Hats microservice

I created a Hat Model to hold all the properties of each hat in the database as well as LocationVO Model to poll the Location information for each hat from the Wardrobe API.
